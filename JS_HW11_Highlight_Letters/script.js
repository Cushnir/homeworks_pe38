// В поле Input возможен ввод не только с клавиатуры, но и другими способами (голосовой ввод, 
// копи-паст). Поэтому для Input есть более универсальное событие input и change, которые 
// отслеживают любое изменение в поле

const keyButtons = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
    keyButtons.forEach(element => {
        event.code.toLocaleLowerCase() === element.dataset.key ? element.className = 'btn-pressed' : element.className = 'btn';
    });
})
