
/*Опишите своими словами, что такое метод обьекта

Метод объекта - это такое свойство-действие. Т.е., у простых свойств есть значения-данные любого типа,
которые мы можем читать и записывать. А метод объекта может оперировать этими данными.
Обычно методы это функции.*/

function createNewUser() {
    let userFirstName = prompt("Enter Your Name");
    let userLastName = prompt("Enter Your Lastname");
    return {
        _firstName: userFirstName,
        _lastName: userLastName,
        
        // set firstName(value) {
        //     this._firstName = value;
        // },
        set lastName(userLastName) {
            this._lastName = userLastName;
        },
        getLogin() {
            return `${this._firstName[0].toLowerCase()}${this._lastName.toLowerCase()}`;
        }
    }
}
let myUser = createNewUser(); // Создали объект
console.log(myUser);
Object.defineProperty(myUser, "_firstName", {set firstName(value) {
    this._firstName = value;
}});
Object.defineProperty(myUser, "_lastName", {});


myUser._firstName = "TEST1"; // Попытались напрямую перезаписать имя на другое. 
myUser._lastName = "TEST2"; // Попытались напрямую перезаписать фамилию на другую. 
console.log(myUser); // Не смотря на попытки перезаписать имя / фамилию - они остались старыми,
console.log(`${myUser.getLogin()}`);

myUser.firstName("TEST1"); // Пытаемся перезаписать имя через метод
myUser.lastName("TEST2"); // Пытаемся перезаписать фамилию через метод
console.log(myUser); // Имя и фамилия теперь новые, потому что записаны через методы setFirstName и setLastName
console.log(`${myUser.getLogin()}`);






// function createNewUser() {
//     let userFirstName = prompt("Enter Your Name");
//     let userLastName = prompt("Enter Your Lastname");
//     return {
//         _firstName: userFirstName,
//         _lastName: userLastName,
//         setFirstName(userFirstName) {
//             this._firstName = userFirstName;
//         },
//         setLastName(userLastName) {
//             this._lastName = userLastName;
//         },
//         getLogin(_firstName, _lastName) {
//             return `${this._firstName[0].toLowerCase()}${this._lastName.toLowerCase()}`;
//         }
//     }
// }

// let myUser = createNewUser(); // Создали объект
// myUser._firstName = "TEST1"; // Попытались напрямую перезаписать имя на другое. 
// myUser._lastName = "TEST2"; // Попытались напрямую перезаписать фамилию на другую. 
// console.log(myUser); // Не смотря на попытки перезаписать имя / фамилию - они остались старыми,
// console.log(`${myUser.getLogin()}`);

// myUser.setFirstName("TEST1"); // Пытаемся перезаписать имя через метод
// myUser.setLastName("TEST2"); // Пытаемся перезаписать фамилию через метод
// console.log(myUser); // Имя и фамилия теперь новые, потому что записаны через методы setFirstName и setLastName
// console.log(`${myUser.getLogin()}`);
