//Рекурсия - это когда функция внутри себя вызывает саму себя.
// Это удобно, когда нужно вычислить какие то циклические преобразования, наподобие 
// каких-либо прогрессий. Иначе, есть вариант использовать циклы.
// Использование рекурсий короче чем циклы, в плане кода, но из-за создания
// стека (каждый шаг рекурсии кладется в стек) это затратно по ресурсам.
// Если неосторожно устроить краш-тест, можно наверное комп повесить)



let number = +prompt('Enter some number:');

function factorial(n) {
    if (n == 1) {
        return n
    } 
    return  n * factorial(n-1);
}

alert('factorial n = ' + factorial(number));

// Если устроить проверку на правильность ввода, тогда все ок.
// Если пользователь введет не число, возвращает undefined.
// Это я заметил, когда забыл преобразовать введенную строку через prompt в число.