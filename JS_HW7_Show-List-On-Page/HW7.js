// DOM- это структура HTML страницы. Она имеет дерево-подобный вид, в корне которого
// находится объект document. Все элементы на странице являются его потомками. 
// DOM формирует браузер во время загрузки страницы.
// Теги и текстовые блоки являются узлами DOM. К ним можно обращаться, менять свойства,
// делать поиск по заданным критериям, добавлять новые и удалять. 




let itemsArray = ["hello", "World", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function addListOnPage(arr, parentTag = document.body) {
    parentTag.insertAdjacentHTML('afterbegin', `<ul>${arr.map(item => {
        return `<li>${item}</li>`
    }).join('')}</ul>`);
}

addListOnPage(itemsArray);




//  ДОПОЛНИТЕЛЬНАЯ СЛОЖНОСТЬ. Смог решить сначала только через forEach.
//  и это помогло  уже реализовать через map (вариант ниже) 

// let itemsArray = ["A111", "A222", ["B111", "B222"], "A333", "A444"];

// function addListOnPage(arr, parentTag = document.body) {
//     let ul = document.createElement('ul');
//     arr.forEach(elem => {
//         let li = document.createElement('li');
//         if (Array.isArray(elem)) { //если елемент  массив
//             addListOnPage(elem, li);
//             ul.appendChild(li);
//         }
//         else {
//             li.innerHTML = elem;
//             ul.appendChild(li);
//         }
//     });
//     parentTag.append(ul);
// }
// addListOnPage(itemsArray);





// Вариянт через map     

// let itemsArray = ["A111", "A222", ["B111", "B222"], "A333", "A444"];

// function addListOnPage(arr, parentTag = document.body) {
//     let ul = document.createElement('ul');
//     parentTag.insertAdjacentHTML('afterbegin', `${arr.map(elem => {
//         let li = document.createElement('li');
//         if (Array.isArray(elem)) {
//             addListOnPage(elem, li);
//             ul.appendChild(li);
//         }
//         else {
//             li.innerHTML = elem;
//             ul.appendChild(li);
//         }
//         parentTag.append(ul);
//     }).join('')}`);
// }
// addListOnPage(itemsArray);
