
//  forEach - это встроенный метод перебора массива. Им можно перебрать каждый элемент, и проверить
//  совпадает ли он по какому то условию. И затем сформировать новый массив из подходящих 
//  элементов. 


//  Вариант 1. метод forEach//
let listTypeJS = ['number', 'string', 'boolean', 'null', 'undefined', 'object']; // список типов JS
let beforeFilterArray = [22, 'STRING_ITEM', true, null, undefined, { a: 11, b: 22 }]; // перебираемый массив

function filterBy(myArr, typeOfItems) {
    let myArrFiltered = new Array();
    myArr.forEach(element => {
        if (!(typeof element === typeOfItems)) {
            myArrFiltered.push(element);
        }
    });
    return myArrFiltered;
}

for(let i = 0; i <= listTypeJS.length - 1; i++){
    let afterFilterArray = filterBy(beforeFilterArray, listTypeJS[i]);
    console.log('New Array without ' + listTypeJS[i]);
    console.log(afterFilterArray);
}

// // Вариант 2. метод filter //
// let listTypeJS = ['number', 'string', 'boolean', 'null', 'undefined', 'object']; // список типов JS
// let beforeFilterArray = [22, 'STRING_ITEM', true, null, undefined, { a: 11, b: 22 }]; // перебираемый массив

// for (let i = 0; i <= listTypeJS.length - 1; i++) {
//     let afterFilterArray = beforeFilterArray.filter(function (element) {
//         return !(typeof element === listTypeJS[i])
//     });
//     console.log('New Array without ' + listTypeJS[i]);
//     console.log(afterFilterArray);
// }

// //  Вариант 3. метод forEach (тут чисто попробовал ваш вариант, подсмотрел в требованиях //
// //  к заданию. Я до строки (54) не додумаллся). ForEach для обоих массивов. Работает))   //

// let listTypeJS = ['number', 'string', 'boolean', 'null', 'undefined', 'object']; // список типов JS
// let beforeFilterArray = [22, 'STRING_ITEM', true, null, undefined, { a: 11, b: 22 }]; // перебираемый массив

// function filterBy(myArr, typeOfItems) {
//     let myArrFiltered = new Array();
//     myArr.forEach(element => {
//         if (!(typeof element === typeOfItems)) {
//             myArrFiltered.push(element);
//         }
//     });
//     return myArrFiltered;
// }
// listTypeJS.forEach(type => console.log(filterBy(beforeFilterArray, type)));