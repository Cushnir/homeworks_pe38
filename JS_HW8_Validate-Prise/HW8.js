// Обработчик события - это скорее всего механизм отслеживания действий в программе/браузере, который может слушать
// как конкретные действия пользователя, так и действия программы/системы(срабатывание таймеров, загрузка страницы, контента и т.п.)
// Обработчик событий можно назначить на отдельный элемент.

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const divPriceBlock = document.createElement('div');
divPriceBlock.classList.add('price-block-input');
divPriceBlock.innerText = 'Цена, $ ';

const inputPrice = document.createElement('input');
inputPrice.classList.add('price-input');
inputPrice.type = 'number';

document.body.append(divPriceBlock);
divPriceBlock.append(inputPrice);

const spanWrongValue = document.createElement('span');
spanWrongValue.classList.add('lbl-wrongvalue', 'disp-none');
spanWrongValue.innerText = 'Please enter correct price';
divPriceBlock.append(spanWrongValue);

// функция вызывается при потере фокуса на поле ввода
inputPrice.onblur = function () {
    let valueEntered = inputPrice.value; //записываю в переменную значение поля ввода

    if (valueEntered < 0 || valueEntered === "") { // если значение меньше 0        
        spanWrongValue.classList.remove('disp-none');
        this.classList.add('border-price-error');
        this.classList.remove('price-input-valid');
    } else {
        // document.body.querySelector('.price-block-input')
        divPriceBlock.insertAdjacentHTML('beforebegin', `<span class="price-label">Текущая цена, $${valueEntered} 
                            <button class="btn-close-label">X</button></span>`);
        spanWrongValue.classList.add('disp-none');
        this.classList.remove('border-price-error');
        this.classList.add('price-input-valid');
    }
}

// функция вызывается при клике на body, и проверяет, есть ли у вызвавшего событие элемента
// класс btn-close-label, находит его родителя по наличию класса price-label, и уделает его
function closeClickedElement(event) {
    if (event.target.classList.contains('btn-close-label')){
        event.target.closest(".price-label").remove();
    }
}

document.body.addEventListener('click', closeClickedElement);

