// Теоретический вопрос

// Циклы нужны, конда необходимо выполнить одинаковое действие много или заданное количество раз.
//
// Если мы знаем точное колличество выполнений (итераций), можно использовать цикл со счетчиком 
// for.
//
// Когда циклл должен выполняться пока соблюдается условие выполнения, используем циклы 
// while и do..while
//Отличие в том, что while сначала проверяет, а потом выполняет.
//А do..while сначала выполняет. а потом проверяет. И выполняет обязательно хотябы один раз!

//////////////////////////////////////////////////////////////////////////////
// Вариант №1: Только положительные числа с проверкой на целое число.

let userNumber;

while (!Number.isInteger(userNumber)) {
    userNumber = +prompt('Enter some numder:');
}

if (userNumber < 5) {
    alert('Sorry, no numbers');
}

for (let i = userNumber; i >= 5; i--) {
    if (i % 5 === 0) {
        console.log(i);
    }
}


//////////////////////////////////////////////////////////////////////////////
// Вариант №2: Положительные и отрицательные числа с проверкой на целое число.


// let userNumber;
// while (!Number.isInteger(userNumber)) {
//     userNumber = +prompt('Enter some numder:');
// }
// if (userNumber < 5 && userNumber > -5) {
//     alert('Sorry, no numbers');
// } else if (userNumber >= 5) {
//     for (let i = userNumber; i >= 5; i--) {
//         if (i % 5 === 0) {
//             console.log(i);
//         }
//     }
// } else {
//     for (let i = userNumber; i <= -5; i++) {
//         if (i % 5 === 0) {
//             console.log(i);
//         }
//     }
// }


//////////////////////////////////////////////////////////////////////////////
// Дополнительное: Вывод в консоль простых чисел в заданном диапазоне.

// let lowerNumber = +prompt('Enter a lower number');
// let largerNumber = +prompt('Enter a larger number');
// while (lowerNumber >= largerNumber) {
//     lowerNumber = +prompt('Enter a lower number');
//     largerNumber = +prompt('Enter a larger number');
// }
// for (; lowerNumber <= largerNumber; lowerNumber++) {
//     for (let i = 2; i <= lowerNumber; i++) { //пока Меньшее меньше или равно Большего
//         let numberIsSimple = true; // Число Простое
//         for (let tempNumber = 2; tempNumber < lowerNumber; tempNumber++) {
//             if (i % tempNumber === 0) {
//                 numberIsSimple = false;
//                 break;
//             }
//         }
//         if (numberIsSimple) {
//             console.log(i);
//         }
//     }
// }