// Меняем "глазки"
const formPass = document.querySelector('.password-form'); //получаем форму
const iconEyeOne = document.getElementById('eye-one');  //получаем "глазки"
const iconEyeTwo = document.getElementById('eye-two');
const inputPassOne = document.getElementById('input-one'); //получаем инпуты
const inputPassTwo = document.getElementById('input-two');
const submitBtn = document.querySelector('.btn'); // получаем кнопку
const wrongPass = document.createElement('span'); //создаем спан 
wrongPass.innerText = 'Нужно ввести одинаковые значения'; //вставляем текст в спан

formPass.addEventListener('click', (event) => {
    if (event.target === iconEyeOne || event.target === iconEyeTwo) {
        iconEyeOne.classList.toggle('fa-eye-slash');
        iconEyeTwo.classList.toggle('fa-eye-slash');
        if (inputPassOne.getAttribute('type') === 'password' && inputPassTwo.getAttribute('type') === 'password') {
            inputPassOne.setAttribute('type', 'text');
            inputPassTwo.setAttribute('type', 'text');
        } else {
            inputPassOne.setAttribute('type', 'password');
            inputPassTwo.setAttribute('type', 'password');
        }
    }
})

// кнопка Подтвердить
submitBtn.addEventListener('click', (event) => {
    event.preventDefault(); // отменяем действие по умолчанию
    if (inputPassOne.value != inputPassTwo.value || inputPassOne.value === '' || inputPassTwo.value === '') {
        wrongPass.classList.add('span-visible');
        wrongPass.classList.remove('disp-none');
        inputPassTwo.after(wrongPass); // добавляем элемент "снаружи после" указанного элемента
    } else if (inputPassOne.value === inputPassTwo.value) { // сравниваем введенные значения
        wrongPass.classList.add('disp-none');
        //без таймаута сначала показывается алерт, а только потом уберается надпись
        setTimeout(() => alert('You are welcome'), 100);
    }
})



