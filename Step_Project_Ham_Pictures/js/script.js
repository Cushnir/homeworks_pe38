
/* переключение табов секции Our Services (здесь нет дилегирования. можно сделать рефакторинг
   с дилегированием, как в секции Our Amazing Work)  */
let tabsOurservList = document.querySelectorAll('.tabs-ourserv li');
tabsOurservList.forEach(element => {
    element.addEventListener('click', function () {
        document.querySelector('.tabs-ourserv li.active').classList.remove('active');
        document.querySelector(`.tabs-ourserv li[data-tab-ourserv="${element.dataset.tabOurserv}"]`).classList.add('active');
        document.querySelector('.tabs-ourserv-content li.active').classList.remove('active');
        document.querySelector(`.tabs-ourserv-content li[data-tab-ourserv="${element.dataset.tabOurserv}"]`).classList.add('active');
    })
})


/* показываем картинки по категориям в секции Our Amazing Work */
let workChangeImgGategory = document.querySelector('.tabs-work');
let workImgSet = document.querySelectorAll('.work-img');
let tabsWorkTitle = document.querySelectorAll('.tabs-work-title');

// случайное целое число в диапазоне. (эту функцию нашел в интернете)
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
// функция показывает 12 случайных картинок
function showAllRandomImgSet(nodeCollection, addRemoveClass) {
    nodeCollection.forEach((elem) => {
        elem.classList.add(addRemoveClass);
    })
    for (let i = 1; i <= 12; i++) {
        let randIndx = getRandomInt(0, (nodeCollection.length - 1));
        nodeCollection[randIndx].classList.remove(addRemoveClass);
    }
}

showAllRandomImgSet(workImgSet, 'work-img-dispnone');

workChangeImgGategory.addEventListener('click', (event) => {
    if (event.target.tagName == 'LI') {
        // меняем стиль активного таба
        tabsWorkTitle.forEach(element => {
            if (element.classList.contains('active')) {
                element.classList.remove('active');
            } else {
                event.target.classList.add('active');
            }
        })
        // меняем сет картинок по категориям
        if (event.target.dataset.imgset === "all") {
            showAllRandomImgSet(workImgSet, 'work-img-dispnone')
        } else {
            workImgSet.forEach((elem) => {
                if (event.target.dataset.imgset === elem.dataset.imgset) {
                    elem.classList.remove('work-img-dispnone');
                }
                else if (event.target.dataset.imgset != elem.dataset.imgset) {
                    elem.classList.add('work-img-dispnone');
                }
            })
        }
    }
})

// карусель -----------------------------------------------------
const peopleAll = document.querySelector(".people-all");
let peopleCount = document.querySelectorAll(".singl-people").length;
const controller = document.querySelector(".people-controller-wrapper");
const btnPrev = document.querySelector(".btn-previous");
const btnNext = document.querySelector(".btn-next");
let position = 0;
let positionMove = peopleAll.offsetWidth;

let checkBtns = () => {
    if (position == 0) {
        btnPrev.classList.add('disabled-btn');
    }
    else if (position < -(positionMove * peopleCount - positionMove * 2)) {
        btnNext.classList.add('disabled-btn');
    }
    else if (position < 0 || position > -(positionMove * peopleCount - positionMove * 2)) {
        btnPrev.classList.remove('disabled-btn');
        btnNext.classList.remove('disabled-btn');
    }
    
};


// управление
controller.addEventListener('click', (event) => {
    if (event.target.classList.contains('btn-previous')) {
        position = position + positionMove;
        peopleAll.style.transform = `translateX(${position}px)`;

    } else
    if (event.target.classList.contains('btn-next')) {
        position = position - positionMove;
        peopleAll.style.transform = `translateX(${position}px)`;
        console.log(event.target);
    }
    checkBtns();
})

checkBtns();
