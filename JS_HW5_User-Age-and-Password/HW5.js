
/*Теоретический вопрос

Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования


Задание
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. 
Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и 
дополните ее следующим функционалом:

При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) 
и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в 
верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. 
(например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и 
getPassword() созданного объекта.*/

function createNewUser() {
    let userFirstName = prompt("Enter Your Name");
    let userLastName = prompt("Enter Your Lastname");
    let userBirthDay = prompt("Enter Your Birth Day (use format dd.mm.yyyy)");
    return {
        _firstName: userFirstName,
        _lastName: userLastName,
        birthday: userBirthDay,
        setFirstName(userFirstName) {
            this._firstName = userFirstName;
        },
        setLastName(userLastName) {
            this._lastName = userLastName;
        },
        getAge() {
            let newDate = new Date();
            let userAge = newDate.getFullYear() - (+this.birthday.slice(6));
            if (+this.birthday.slice(3, 5) > (newDate.getMonth() + 1)) {
                return userAge -= 1;
            } else
                if (+this.birthday.slice(3, 5) == (newDate.getMonth() + 1) &&
                    +this.birthday.slice(0, 2) > newDate.getDate()) {
                    return userAge -= 1;
                }
            return userAge;
        },
        getPassword() {
            return `${this._firstName[0].toUpperCase()}${this._lastName.toLowerCase()}${this.birthday.slice(6)}`
        },
    }
}

const newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

// newUser.getLogin = function (firstName, lastName) {
//     return `${this._firstName[0].toLowerCase()}${this._lastName.toLowerCase()}`
// }

// console.log(`${newUser.getLogin()}`);


